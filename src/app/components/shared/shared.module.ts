import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardsComponent } from './cards/cards.component';
import { MainComponent } from './main/main.component';



@NgModule({
  declarations: [
    CardsComponent,
    MainComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    MainComponent,
    CardsComponent
  ],
})
export class SharedModule { }
